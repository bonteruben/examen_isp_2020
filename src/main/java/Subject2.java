import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Subject2 {

    public static class GUI extends JFrame {

        JTextField input;
        JTextField output;

        GUI() {
            this.setLocationRelativeTo(null);

            this.setSize(200, 200);

            JButton write = new JButton("Write");
            write.setAlignmentX(Box.CENTER_ALIGNMENT);

            write.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    String t = input.getText();
                    output.setText(t);
                }
            });

            input = new JTextField(10);
            output = new JTextField(10);
            output.setEditable(false);

            JPanel text = new JPanel();
            text.add(input);
            text.add(output);

            this.getContentPane().add(text);
            this.getContentPane().add(write);

            this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));

            this.setTitle("Subject 2");
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setVisible(true);
        }
    }

    public static void main(String[] args) {
        new GUI();
    }
}
