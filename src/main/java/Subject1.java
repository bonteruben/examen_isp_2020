public class Subject1 {

    public class B {

        private long t;
        private C c;

        public B(long t) {
            this.t = t;
        }

        public void b(C c) {

        }
    }

    public class A extends B {

        public A(long t) {
            super(t);
        }
    }

    public class C {

    }

    public class D {

        private E e = new E();

        public void met1(int i) {

        }
    }

    public class E {

        public void met2() {

        }
    }

    public class F {

        private D d;

        public F(D d) {
            this.d = d;
        }
    }
}
